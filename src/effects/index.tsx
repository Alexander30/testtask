import { Deviation } from "../components/Deviation/Deviation";
import React         from "react";

const head = ['Подразделение', 'Сегмент', 'Валюта', 'Отклонение от плана ПП'];

const rows = [
  ['Консолидация', 'КБ', 'Валюта', <Deviation rejection={-50} />],
  ['Дочерние организации', 'КБ', 'Валюта', <Deviation rejection={-30} />],
  ['Консолидация', 'КБ', 'Валюта', <Deviation rejection={10} />],
  ['Консолидация', 'КБ', 'Валюта', <Deviation rejection={19} />]
];

const getData = async () => {
  const response = await fetch('/data.json');
  const json = await response.json();
  return json;
};

/** Todo typing for data **/
const getHead = (data: any) => {
  const headFromRequest = data.fa.fa_data.axis.r.map((name: any) => ({value: name.sAxisName, sorted: false}));
  return headFromRequest.concat(
    [
      {value: 'Валюта', sorted: false},
      {value: 'Отклонение от плана П.П.', sorted: false}
    ]);
};

/** TODO typings **/
const getMaxPlanAbsValue = (data: any) => {
  return data.reduce((a: any, v: any) => Math.abs(v.fDeltaPlan) > Math.abs(a) ? Math.abs(v.fDeltaPlan) : a, 0);
}

/** TODO typing for data **/
const getRows = (data: any) => {
  const maxPlan = getMaxPlanAbsValue(data.fa.fa_data.r);
  const trueRow = data.fa.fa_data.r.map(
    (row: any) => {
      const result = row.axis.r.map((v: any) => {
          const value = v.sName_RU.length > 17 ? v.sName_RU.slice(0, 17) + '...' : v.sName_RU;
          return {
            value: value,
            sortBy: value
          }
        }
      );

      const deviationVal = row.fDeltaPlan / maxPlan;
      const result2 = result.concat([
        {
          value: row.sMeasDelta_RU,
          sortBy: row.sMeasDelta_RU
        },
        {
          value: <Deviation rejection={parseFloat(((deviationVal) * 100).toFixed(2))} />,
          sortBy: deviationVal
        }
      ]);

      return result2;
    }
  );

  return trueRow;
};

const sortByAlphabet = (a: string, b: string, sorted:string|boolean): number => {
  const A=a.toLowerCase();
  const B=b.toLowerCase();
  if (sorted === 'asc' || !sorted) {
    if (A < B) return -1 ;
    if (A > B) return 1;
    return 0;
  }
  if (sorted === 'desc') {
    if (A < B) return 1 ;
    if (A > B) return -1;
    return 0;
  }
  return 0;

};

const sortByNumber = (a: number, b: number, sorted: string|boolean): number => {
  switch (sorted) {
    case 'desc':
      return b - a;
    case 'asc':
      return a - b;
    default:
      return 0;
  }
};

const sortyByColumn = (currentState:any, columnNumber:number, sorted:any) => {
  return currentState.sort((a: any, b: any) => {
    return typeof b[columnNumber].value === 'string' ?
      sortByAlphabet(a[columnNumber].sortBy, b[columnNumber].sortBy, sorted) :
      sortByNumber(a[columnNumber].sortBy, b[columnNumber].sortBy, sorted);
  });
}

export const sortData = (oldState:any, columnNumber:number) => {
  const currentData = oldState.data!.rows;
  const sorted = oldState.data!.head[columnNumber]!.sorted;
  const nextSorted = ((sorted) => {
    if (!sorted) return 'asc';
    if (sorted === 'asc') return 'desc';
    if (sorted === 'desc') return 'asc';
  })(sorted);
  const sortedData = sortyByColumn(currentData, columnNumber, nextSorted);
  const newData = Object.assign({}, oldState.data);
  newData.rows = sortedData;
  
  newData.head.map((v: any, i:number) => {
    newData.head[i].sorted = false;
  });

  newData.head[columnNumber].sorted = nextSorted;
  return newData;
}

/** TODO process error **/
export const appStateMounted = async () => {
  const data = await getData();
  const head = getHead(data);
  const rows = getRows(data);
  return {loaded: true, data: {rows, head}};
};