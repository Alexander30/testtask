import React, { Component } from 'react';
import './App.css';

import { Table }                     from "./components/Table/Table";
import { appStateMounted, sortData } from "./effects";
import { TBodyRow, THeadRow }        from "./typings";

/**
 * :) :) :) времени нормально прописать тайпинги не было
 *
 * вот в этом файлике еще кое-как сделал, чтобы показать что умею
 *
 * дальше треш и угар, конечно.
 *
 */


interface AppData {
  head: THeadRow[],
  rows: TBodyRow[][]
}

interface Props {
  className?: string;
}

interface State {
  loaded: boolean,
  error: boolean | string,
  data: AppData | null
}

class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {error: false, loaded: false, data: null};
    this.sortByColumn = this.sortByColumn.bind(this);
  }


  componentDidMount() {
    appStateMounted().then(res => this.setState(res));
  }

  sortByColumn(columnNumber: number) {
    const newState = sortData(this.state, columnNumber);
    this.setState({data: newState})
  }

  render() {
    const {loaded, data, error} = this.state;
    return (
      <div className="App">
        {loaded && <Table head={data!.head} rows={data!.rows} sort={this.sortByColumn} />}
        {!loaded && 'Loading...'}
        {error}
      </div>
    );
  }
}

export default App;
