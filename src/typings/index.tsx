import { ReactNode } from "react";

export interface THeadRow {
  value: 'string',
  sorted: 'ask' | 'desc' | false
}

export interface TBodyRow {
  value: string | ReactNode,
  sortBy: string | number
}