import * as React      from 'react';
import classNames from 'classnames';

import styles from './.module.scss';

interface Props {
  className?: string;
  align: 'left'|'right';
  color: string;
  width:string;
}

export function Scale({className, color, width, align}: Props) {
  return (
    <div className={classNames(styles.scale, className)} style={{textAlign: align}}>
      <span style={{background: color, width: width}}/>
    </div>

  );
}