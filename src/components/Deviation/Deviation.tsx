import * as React from 'react';
import classNames from 'classnames';
// @ts-ignore
import styles     from "./.module.scss";
import { Scale }  from "../Scale/Scale";

interface Props {
  className?: string;
  rejection: number
}

export function Deviation({className, rejection}: Props) {
  const absRejection = Math.abs(rejection);
  const rejectionType = rejection >= 0 ? 'positive' : 'negative';

  return (
    <div className={classNames(styles.deviation, classNames)}>
      <div className={styles['scale-container']}>
        {rejectionType === 'negative' &&
        <Scale className={styles.scale} align={'right'} color={'#db4953'} width={`${absRejection}%`} />}
      </div>
      <span className={styles.rejection}> {rejection} п.п.</span>
      <div className={styles['scale-container']}>
        {rejectionType === 'positive' &&
        <Scale className={styles.scale} align={'left'} color={'#3aab6b'} width={`${absRejection}%`} />}
      </div>
    </div>
  );
}