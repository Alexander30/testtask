import * as React from 'react';
import classNames from 'classnames';

// @ts-ignore
import styles                 from './.module.scss';
import { TBodyRow, THeadRow } from "../../typings";

interface Props {
  className?: string;
  head: THeadRow[],
  rows: TBodyRow[][],
  sort: (columnNumber:number) => void
}

interface TableRow {
  row: string[]
}

/**TODO write typings **/
const Row = ({row}: any) => <tr>{row.map((v: any, item:number) => <td key={item}>{v.value}</td>)}</tr>;

export function Table({className, head, rows, sort}: Props) {
  return (
    <div className={classNames(className)}>
      <table className={styles.table} cellSpacing="0" cellPadding="0">
        <thead className={styles.thead}>
          <tr>
            {head.map((v:any, item) => <th onClick={() => {sort(item)}} key={item}>{v.value}</th>)}
          </tr>
        </thead>
        <tbody>
        {rows.map((row:TBodyRow[], item:number) => <Row row={row} key={item} />)}
        </tbody>
      </table>
    </div>
  );
}